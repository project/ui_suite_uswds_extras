# UI Suite USWDS Extras

This module is to provide additional patterns that USWDS centric but didn't fit
into the main UI Suite USWDS theme.

## Installation

Install as you would normally install a contributed Drupal theme. See
https://www.drupal.org/node/1897420 for further information.

## Available patterns

TBD
